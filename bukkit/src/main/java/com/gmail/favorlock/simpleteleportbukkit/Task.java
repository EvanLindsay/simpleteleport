package com.gmail.favorlock.simpleteleportbukkit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Task extends BukkitRunnable {
	
	SimpleTeleportBukkit plugin;
	String channel;
	Player player;
	byte[] data;
	
	public Task(SimpleTeleportBukkit plugin, String channel, Player player, byte[] data) {
		this.plugin = plugin;
		this.channel = channel;
		this.player = player;
		this.data = data;
	}

	@Override
	public void run() {
		if (channel.equals("SimpleTeleport")) {
			DataInputStream in = new DataInputStream(new ByteArrayInputStream(data));
			String subchannel = null;
			
			try {
				subchannel = in.readUTF();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			if ((subchannel.equals("TeleportLocal")) || (subchannel.equals("TeleportRemote"))) {
				Player client = null;
				Player target = null;
				
				try {
					client = Bukkit.getPlayer(in.readUTF());
					target = Bukkit.getPlayer(in.readUTF());
				} catch (IOException e) {
				e.printStackTrace();
				}
				
				if ((Bukkit.getPluginManager().isPluginEnabled("Vault")) && 
					(SimpleTeleportBukkit.permission.getPrimaryGroup(target).equalsIgnoreCase("Jailed"))) {
					return;
				}
				
				if ((target != null) && (client != null)) {
					Location destination = (Location) client.getLocation();
					target.teleport(destination);
					return;
				}
				return;
			}
			if (subchannel.equals("TeleportCheck")) {
				String server = null;
				Player target = null;
				
				try {
					server = in.readUTF();
					target = Bukkit.getPlayer(in.readUTF());
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				ByteArrayOutputStream b = new ByteArrayOutputStream();
				DataOutputStream out = new DataOutputStream(b);
				
				if ((Bukkit.getPluginManager().isPluginEnabled("Vault")) &&
					(SimpleTeleportBukkit.permission.getPrimaryGroup(target).equalsIgnoreCase("Jailed"))) {
					return;
				}
				
				try {
					out.writeUTF("Connect");
					out.writeUTF(server);
				} catch (IOException e) {
					e.getStackTrace();
				}
				
				target.sendPluginMessage(this.plugin, "BungeeCord", b.toByteArray());
			}
		} else {
			return;
		}
	}

}
