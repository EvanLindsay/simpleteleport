package com.gmail.favorlock.simpleteleportbukkit;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitTask;

public class SimpleTeleportBukkit extends JavaPlugin implements PluginMessageListener {
	
	public static Permission permission = null;
	
	public void onEnable() {
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "SimpleTeleport", this);
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
			setupPermissions();
		}
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] data) {
		BukkitTask task = new Task(this, channel, player, data).runTaskLater(this, 5L);
	}
	
	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
		if (permissionProvider != null) {
			permission = (Permission)permissionProvider.getProvider();
		}
		return permission != null;
	}

}
