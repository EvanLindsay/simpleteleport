package com.gmail.favorlock.simpleteleport.tasks;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class DelayedTask implements Runnable {
	
	ProxiedPlayer player;
	byte[] bArray;
	
	public DelayedTask(ProxiedPlayer player, byte[] bArray) {
		this.player = player;
		this.bArray = bArray;
	}

	public void run() {
		player.getServer().sendData("SimpleTeleport", bArray);
	}

}
