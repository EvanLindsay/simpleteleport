package com.gmail.favorlock.simpleteleport.cmd.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;
import com.gmail.favorlock.simpleteleport.cmd.BaseCommand;
import com.gmail.favorlock.simpleteleport.tasks.TeleUtil;
import com.gmail.favorlock.simpleteleport.utils.FontFormat;

public class TeleportAccept extends BaseCommand {
	
	private SimpleTeleport plugin;

	public TeleportAccept(SimpleTeleport plugin) {
		super("tpaccept");
		this.plugin = plugin;
		setDescription("Accept a teleport request");
		setUsage("/tpaccept [player]");
		setArgumentRange(0, 1);
		setPermission("simpleteleport.tpaccept");
		setIdentifiers(new String[] { "tpaccept" });
	}

	@Override
	public boolean execute(CommandSender sender, String identifier,
			String[] args) {
		if (!(sender instanceof ProxiedPlayer)) {
			return false;
		}
		ProxiedPlayer player = (ProxiedPlayer) sender;
		ProxiedPlayer target = null;
		
		if (args.length == 1) {
			for (ProxiedPlayer user : ProxyServer.getInstance().getPlayers()) {
				if (user.getName().equalsIgnoreCase(args[0])) {
					target = user;
				}
			}
		
			if (target == null) {
				player.sendMessage(FontFormat.translateString("&a" + args[0] + " &6is not online!"));
				return false;
			}
		}
		
		boolean containsPlayer = plugin.getRequest().containsKey(player.getName().toLowerCase());
		boolean containsPlayerHere = plugin.getHereRequest().containsKey(player.getName().toLowerCase());
		
		if (!containsPlayer && !containsPlayerHere) {
			player.sendMessage(FontFormat.translateString("&6You have no teleportation request!"));
			return false;
		}
		if (!(player == null) && !(target == null)) {
			boolean teleport = false;
			if (player.getServer().getInfo().getName() != target.getServer().getInfo().getName()) {
				if (containsPlayer) {
					boolean containsTarget = plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase());
					if (containsTarget) {
						TeleUtil.teleportRemote(plugin, player, target);
						if (plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
							plugin.getRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
						}
						if(plugin.getRequest().get(player.getName().toLowerCase()).isEmpty()) {
							plugin.getRequest().remove(player.getName().toLowerCase());
						}
						teleport = true;
					} else {
						teleport = false;
					}
				} 
				if (containsPlayerHere && !teleport) {
					boolean containsTarget = plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase());
					if (containsTarget) {
						TeleUtil.teleportRemote(plugin, target, player);
						if (plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
							plugin.getHereRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
						}
						if(plugin.getHereRequest().get(player.getName().toLowerCase()).isEmpty()) {
							plugin.getHereRequest().remove(player.getName().toLowerCase());
						}
						teleport = true;
					} else {
						teleport = false;
					}
				}
				if (teleport == true) {
					sender.sendMessage(FontFormat.translateString("&6Teleport request accepted."));
					return true;
				} else {
					sender.sendMessage(FontFormat.translateString("&6You have no teleportation request from &a" + args[0] + "&6!"));
					return false;
				}
			} else {
				if (containsPlayer) {
					boolean containsTarget = plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase());
					if (containsTarget) {
						TeleUtil.teleportLocal(player, target);
						if (plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
							plugin.getRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
						}
						if(plugin.getRequest().get(player.getName().toLowerCase()).isEmpty()) {
							plugin.getRequest().remove(player.getName().toLowerCase());
						}
						teleport = true;
					} else {
						teleport = false;
					}
				} 
				if (containsPlayerHere && !teleport) {
					boolean containsTarget = plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase());
					if (containsTarget) {
						TeleUtil.teleportLocal(target, player);
						if (plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
							plugin.getHereRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
						}
						if(plugin.getHereRequest().get(player.getName().toLowerCase()).isEmpty()) {
							plugin.getHereRequest().remove(player.getName().toLowerCase());
						}
						teleport = true;
					} else {
						teleport = false;
					}
				}
				if (teleport == true) {
					sender.sendMessage(FontFormat.translateString("&6Teleport request accepted."));
					return true;
				} else {
					sender.sendMessage(FontFormat.translateString("&6You have no teleportation request from &a" + args[0] + "&6!"));
					return false;
				}
			}
		} else if (!(player == null) && args.length == 0) {
			String request = plugin.findFirstRequestAvailable(player.getName().toLowerCase());
			
			if (request != null) {
				target = ProxyServer.getInstance().getPlayer(request);
				if (player.getServer().getInfo().getName().equals(target.getServer().getInfo().getName())) {
					TeleUtil.teleportLocal(player, target);
					removeFromList(player, target);
					return true;
				} else {
					TeleUtil.teleportRemote(plugin, player, target);
					removeFromList(player, target);
					return true;
				}
			}
			
			request = plugin.findFirstRequestAvailableHere(player.getName().toLowerCase());
			
			if (request != null) {
				target = ProxyServer.getInstance().getPlayer(request);
				if (player.getServer().getInfo().getName().equals(target.getServer().getInfo().getName())) {
					TeleUtil.teleportLocal(target, player);
					removeFromListHere(player, target);
					return true;
				} else {
					TeleUtil.teleportRemote(plugin, player, target);
					removeFromListHere(player, target);
					return true;
				}
			}
		}
		return false;
	}
	
	public void removeFromList(ProxiedPlayer player, ProxiedPlayer target) {
		if (plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
			plugin.getRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
		}
		if(plugin.getRequest().get(player.getName().toLowerCase()).isEmpty()) {
			plugin.getRequest().remove(player.getName().toLowerCase());
		}
	}
	
	public void removeFromListHere(ProxiedPlayer player, ProxiedPlayer target) {
		if (plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
			plugin.getHereRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
		}
		if(plugin.getHereRequest().get(player.getName().toLowerCase()).isEmpty()) {
			plugin.getHereRequest().remove(player.getName().toLowerCase());
		}
	}
}
