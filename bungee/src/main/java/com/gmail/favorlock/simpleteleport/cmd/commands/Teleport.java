package com.gmail.favorlock.simpleteleport.cmd.commands;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;
import com.gmail.favorlock.simpleteleport.cmd.BaseCommand;
import com.gmail.favorlock.simpleteleport.tasks.TeleUtil;
import com.gmail.favorlock.simpleteleport.utils.FontFormat;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Teleport extends BaseCommand {
	
	private SimpleTeleport plugin;

	public Teleport(SimpleTeleport plugin) {
		super("tp");
		this.plugin = plugin;
		setDescription("Teleport a player");
		setUsage("/tp <player> <target>");
		setArgumentRange(2, 2);
		setPermission("simpleteleport.teleport");
		setIdentifiers(new String[] { "tp" });
	}

	@Override
	public boolean execute(CommandSender sender, String identifier,
			String[] args) {		
		ProxiedPlayer player = null;
		ProxiedPlayer target = null;
		for (ProxiedPlayer user : ProxyServer.getInstance().getPlayers()) {
			if (user.getName().equalsIgnoreCase(args[0])) {
				player = user;
			}
			if (user.getName().equalsIgnoreCase(args[1])) {
				target = user;
			}
		}
		if (!(player == null) && !(target == null)) {
			if (player.getServer().getInfo().getName() != target.getServer().getInfo().getName()) {
				TeleUtil.teleportRemote(plugin, target, player);
				return true;
			} else {
				TeleUtil.teleportLocal(target, player);
				return true;
			}
		} else {
			sender.sendMessage(FontFormat.translateString("&6You specified an invalid player!"));
			return false;
		}
	}

}
