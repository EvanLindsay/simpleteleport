package com.gmail.favorlock.simpleteleport.cmd.commands;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;
import com.gmail.favorlock.simpleteleport.cmd.BaseCommand;
import com.gmail.favorlock.simpleteleport.tasks.Task;
import com.gmail.favorlock.simpleteleport.utils.FontFormat;

public class TeleportAHere extends BaseCommand {
	
	private SimpleTeleport plugin;

	public TeleportAHere(SimpleTeleport plugin) {
		super("tpahere");
		this.plugin = plugin;
		setDescription("Request to teleport to a player");
		setUsage("/tpahere <target>");
		setArgumentRange(1, 1);
		setPermission("simpleteleport.tpahere");
		setIdentifiers(new String[] { "tpahere" });
	}

	@Override
	public boolean execute(CommandSender sender, String identifier,
			String[] args) {
		if (!(sender instanceof ProxiedPlayer)) {
			return false;
		}
		ProxiedPlayer player = null;
		for (ProxiedPlayer user : ProxyServer.getInstance().getPlayers()) {
			if (user.getName().equalsIgnoreCase(args[0])) {
				player = user;
			}
		}
		if (player == null) {
			sender.sendMessage(FontFormat.translateString("&a" + args[0] + "&6 is not online!"));
			return false;
		}
		if (plugin.getHereRequest().containsKey(player.getName().toLowerCase())) {
			if (plugin.getHereRequest().get(player.getName().toLowerCase()).contains(sender.getName().toLowerCase())) {
				sender.sendMessage(FontFormat.translateString("&6You already sent a request to &a" + args[0] + "&6!"));
				return false;
			} else {
				plugin.getHereRequest().get(player.getName().toLowerCase()).add(sender.getName().toLowerCase());
				sender.sendMessage(FontFormat.translateString("&6Sending teleport request to &a" + args[0] + "&6!"));
				player.sendMessage(FontFormat.translateString("&a" + sender.getName() + "&4 has requested to teleport to you."));
				player.sendMessage(FontFormat.translateString("&6To teleport, type &c/tpaccept &a[" + sender.getName() + "]&6."));
				player.sendMessage(FontFormat.translateString("&6To deny this request, type &c/tpdeny &a[" + sender.getName() + "]&6."));
				ScheduledTask task = ProxyServer.getInstance().getScheduler().schedule(plugin, 
					new Task(plugin,sender.getName(),args[0],"from"),
					30, TimeUnit.SECONDS);
				return true;
			}
		} else {
			final String target = sender.getName().toLowerCase();
			plugin.getHereRequest().put(player.getName().toLowerCase(), new ArrayList<String>(){{ add(target); }});
			sender.sendMessage(FontFormat.translateString("&6Sending teleport request to &a" + args[0] + "&6!"));
			player.sendMessage(FontFormat.translateString("&a" + sender.getName() + " &6would like to teleport to you!"));
			player.sendMessage(FontFormat.translateString("&6To teleport, type &c/tpaccept &a[" + sender.getName() + "]&6."));
			player.sendMessage(FontFormat.translateString("&6To deny this request, type &c/tpdeny &a[" + sender.getName() + "]&6."));
			ScheduledTask task = ProxyServer.getInstance().getScheduler().schedule(plugin, 
					new Task(plugin,sender.getName(),args[0],"from"),
					30, TimeUnit.SECONDS);
			return true;
		}
	}

}
