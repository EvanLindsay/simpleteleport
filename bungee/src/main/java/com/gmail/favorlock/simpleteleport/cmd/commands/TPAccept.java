package com.gmail.favorlock.simpleteleport.cmd.commands;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class TPAccept extends Command {
	
	SimpleTeleport plugin;

	public TPAccept(SimpleTeleport plugin) {
		super("tpaccept");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		plugin.getCommandHandler().dispatch(sender, this.getName(), args);
	}

}
