package com.gmail.favorlock.simpleteleport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import com.gmail.favorlock.simpleteleport.cmd.CommandHandler;
import com.gmail.favorlock.simpleteleport.cmd.commands.TP;
import com.gmail.favorlock.simpleteleport.cmd.commands.TPA;
import com.gmail.favorlock.simpleteleport.cmd.commands.TPAHere;
import com.gmail.favorlock.simpleteleport.cmd.commands.TPAccept;
import com.gmail.favorlock.simpleteleport.cmd.commands.TPDeny;
import com.gmail.favorlock.simpleteleport.cmd.commands.Teleport;
import com.gmail.favorlock.simpleteleport.cmd.commands.TeleportAHere;
import com.gmail.favorlock.simpleteleport.cmd.commands.TeleportAccept;
import com.gmail.favorlock.simpleteleport.cmd.commands.TeleportDeny;
import com.gmail.favorlock.simpleteleport.cmd.commands.TeleportRequest;

public class SimpleTeleport extends Plugin {
	
	private CommandHandler commandHandler;
	private Map<String, ArrayList<String>> tpRequests;
	private Map<String, ArrayList<String>> tpHereRequests;
	
	public void onEnable() {
		commandHandler = new CommandHandler(this);
		tpRequests = new HashMap<String, ArrayList<String>>();
		tpHereRequests = new HashMap<String, ArrayList<String>>();
		
		registerChannels();
		registerCommands();
	}
	
	public void registerChannels() {
		ProxyServer.getInstance().registerChannel("SimpleTeleport");
	}
	
	public void registerCommands() {
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new TP(this));
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new TPA(this));
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new TPAccept(this));
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new TPDeny(this));
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new TPAHere(this));
		getCommandHandler().addCommand(new Teleport(this));
		getCommandHandler().addCommand(new TeleportRequest(this));
		getCommandHandler().addCommand(new TeleportAccept(this));
		getCommandHandler().addCommand(new TeleportDeny(this));
		getCommandHandler().addCommand(new TeleportAHere(this));
	}
	
	public CommandHandler getCommandHandler() {
		return this.commandHandler;
	}
	
	public Map<String, ArrayList<String>> getRequest() {
		return this.tpRequests;
	}
	
	public boolean checkRequest(String player, String target) {
		if (this.tpRequests.containsKey(player)) {
			return this.tpRequests.get(player).contains(target);
		}
		return false;
	}
	
	public Map<String, ArrayList<String>> getHereRequest() {
		return this.tpHereRequests;
	}
	
	public boolean checkHereRequest(String player, String target) {
		if (this.tpHereRequests.containsKey(player)) {
			return this.tpHereRequests.get(player).contains(target);
		}
		return false;
	}
	
	public String findFirstRequestAvailable(String player) {
		if (this.tpRequests.containsKey(player)) {
			return this.tpRequests.get(player).get(0);
		}
		return null;
	}
	
	public String findFirstRequestAvailableHere(String player) {
		if (this.tpHereRequests.containsKey(player)) {
			return this.tpHereRequests.get(player).get(0);
		}
		return null;
	}
}
