package com.gmail.favorlock.simpleteleport.tasks;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;
import com.gmail.favorlock.simpleteleport.utils.FontFormat;

public class Task implements Runnable {
	
	SimpleTeleport plugin;
	String player;
	String target;
	String direction;
	
	public Task(SimpleTeleport plugin, String player, String target, String direction) {
		this.plugin = plugin;
		this.player = player;
		this.target = target;
		this.direction = direction;
	}

	@Override
	public void run() {
		ProxiedPlayer sender = ProxyServer.getInstance().getPlayer(player);
		
		try {
			if (direction.equals("to")) {
				if (plugin.getRequest().get(target.toLowerCase()).contains(player.toLowerCase())) {
					plugin.getRequest().get(target.toLowerCase()).remove(player.toLowerCase());
					sender.sendMessage(FontFormat.translateString("&7Your request to &a" + target + "&7 has expired!"));
				}
				if(plugin.getRequest().get(target.toLowerCase()).isEmpty()) {
					plugin.getRequest().remove(target.toLowerCase());
				}
			} else if (direction.equals("from")) {
				if (plugin.getHereRequest().get(target.toLowerCase()).contains(player.toLowerCase())) {
					plugin.getHereRequest().get(target.toLowerCase()).remove(player.toLowerCase());
					sender.sendMessage(FontFormat.translateString("&7Your request to &a" + target + "&7 has expired!"));
				}
				if(plugin.getHereRequest().get(target.toLowerCase()).isEmpty()) {
					plugin.getHereRequest().remove(target.toLowerCase());
				}
			}
		} catch (NullPointerException e) {
			// Do Nothing
		}
	}

}
