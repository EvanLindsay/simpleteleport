package com.gmail.favorlock.simpleteleport.cmd.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;

public class TPA extends Command {
	
	SimpleTeleport plugin;

	public TPA(SimpleTeleport plugin) {
		super("tpa");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		plugin.getCommandHandler().dispatch(sender, this.getName(), args);
	}

}
