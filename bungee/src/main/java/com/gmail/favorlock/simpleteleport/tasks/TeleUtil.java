package com.gmail.favorlock.simpleteleport.tasks;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class TeleUtil {
	
	public static void teleportLocal(ProxiedPlayer player, ProxiedPlayer target) {
		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
		DataOutputStream output = new DataOutputStream(bStream);
	
		try {
			output.writeUTF("TeleportLocal");
			output.writeUTF(player.getName());
			output.writeUTF(target.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		target.getServer().sendData("SimpleTeleport", bStream.toByteArray());
	}
	
	public static void teleportRemote(SimpleTeleport plugin, ProxiedPlayer player, ProxiedPlayer target) {
		preTeleportCheck(player, target);
		
		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
		DataOutputStream output = new DataOutputStream(bStream);
	
		try {
			output.writeUTF("TeleportRemote");
			output.writeUTF(player.getName());
			output.writeUTF(target.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		ProxyServer.getInstance().getScheduler().schedule(plugin, new DelayedTask(player, bStream.toByteArray()), 
															1, TimeUnit.SECONDS);
	}
	
	public static void preTeleportCheck(ProxiedPlayer player, ProxiedPlayer target) {
		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
		DataOutputStream output = new DataOutputStream(bStream);
		
		try {
			output.writeUTF("TeleportCheck");
			output.writeUTF(player.getServer().getInfo().getName());
			output.writeUTF(target.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		target.getServer().sendData("SimpleTeleport", bStream.toByteArray());
	}

}
