package com.gmail.favorlock.simpleteleport.cmd.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import com.gmail.favorlock.simpleteleport.SimpleTeleport;
import com.gmail.favorlock.simpleteleport.cmd.BaseCommand;
import com.gmail.favorlock.simpleteleport.tasks.TeleUtil;
import com.gmail.favorlock.simpleteleport.utils.FontFormat;

public class TeleportDeny extends BaseCommand {
	
	private SimpleTeleport plugin;

	public TeleportDeny(SimpleTeleport plugin) {
		super("tpdeny");
		this.plugin = plugin;
		setDescription("Deny a teleport request");
		setUsage("/tpdeny [player]");
		setArgumentRange(0, 1);
		setPermission("simpleteleport.tpdeny");
		setIdentifiers(new String[] { "tpdeny" });
	}

	@Override
	public boolean execute(CommandSender sender, String identifier,
			String[] args) {
		if (!(sender instanceof ProxiedPlayer)) {
			return false;
		}
		ProxiedPlayer player = (ProxiedPlayer) sender;
		ProxiedPlayer target = null;
		
		if (args.length == 1) {
			for (ProxiedPlayer user : ProxyServer.getInstance().getPlayers()) {
				if (user.getName().equalsIgnoreCase(args[0])) {
					target = user;
				}
			}
		
			if (target == null) {
				sender.sendMessage(FontFormat.translateString("&a" + args[0] + " &6is not online!"));
				return false;
			}
		}
		
		boolean containsPlayer = plugin.getRequest().containsKey(player.getName().toLowerCase());
		boolean containsPlayerHere = plugin.getHereRequest().containsKey(player.getName().toLowerCase());
		if (!containsPlayer && !containsPlayerHere) {
			sender.sendMessage(FontFormat.translateString("&6You have no teleportation request!"));
			return false;
		}
		if (!(player == null) && !(target == null)) {
			boolean denied = false;
			if (containsPlayer) {
				boolean containsTarget = plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase());
				if (containsTarget) {
					if (plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
						plugin.getRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
					}
					if(plugin.getRequest().get(player.getName().toLowerCase()).isEmpty()) {
						plugin.getRequest().remove(player.getName().toLowerCase());
					}
					denied = true;
				} else {
					denied = false;
				}
			}
			if (containsPlayerHere && !denied) {
				boolean containsTarget = plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase());
				if (containsTarget) {
					if (plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
						plugin.getHereRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
					}
					if(plugin.getHereRequest().get(player.getName().toLowerCase()).isEmpty()) {
						plugin.getHereRequest().remove(player.getName().toLowerCase());
					}
					denied = true;
				} else {
					denied = false;
				}
			}
			if (denied == true) {
				target.sendMessage(FontFormat.translateString("&6Your teleport request to &a" + player.getName() + "&6 has been denied!"));
				sender.sendMessage(FontFormat.translateString("&6You have denied a teleport request from &a" + target.getName() + "&6!"));
				return true;
			} else {
				sender.sendMessage(FontFormat.translateString("&6You have no teleportation request from &a" + args[0] + "&6!"));
				return false;
			}
		} else if (!(player == null) && args.length == 0) {
			String request = plugin.findFirstRequestAvailable(player.getName().toLowerCase());
			
			if (request != null) {
				target = ProxyServer.getInstance().getPlayer(request);
				if (player.getServer().getInfo().getName().equals(target.getServer().getInfo().getName())) {
					target.sendMessage(FontFormat.translateString("&6Your teleport request to &a" + player.getName() + "&6 has been denied!"));
					sender.sendMessage(FontFormat.translateString("&6You have denied a teleport request from &a" + target.getName() + "&6!"));
					removeFromList(player, target);
					return true;
				} else {
					target.sendMessage(FontFormat.translateString("&6Your teleport request to &a" + player.getName() + "&6 has been denied!"));
					sender.sendMessage(FontFormat.translateString("&6You have denied a teleport request from &a" + target.getName() + "&6!"));
					removeFromList(player, target);
					return true;
				}
			}
			
			request = plugin.findFirstRequestAvailableHere(player.getName().toLowerCase());
			
			if (request != null) {
				target = ProxyServer.getInstance().getPlayer(request);
				if (player.getServer().getInfo().getName().equals(target.getServer().getInfo().getName())) {
					target.sendMessage(FontFormat.translateString("&6Your teleport request to &a" + player.getName() + "&6 has been denied!"));
					sender.sendMessage(FontFormat.translateString("&6You have denied a teleport request from &a" + target.getName() + "&6!"));
					removeFromListHere(player, target);
					return true;
				} else {
					target.sendMessage(FontFormat.translateString("&6Your teleport request to &a" + player.getName() + "&6 has been denied!"));
					sender.sendMessage(FontFormat.translateString("&6You have denied a teleport request from &a" + target.getName() + "&6!"));
					removeFromListHere(player, target);
					return true;
				}
			}
		}
		return false;
	}
	
	public void removeFromList(ProxiedPlayer player, ProxiedPlayer target) {
		if (plugin.getRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
			plugin.getRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
		}
		if(plugin.getRequest().get(player.getName().toLowerCase()).isEmpty()) {
			plugin.getRequest().remove(player.getName().toLowerCase());
		}
	}
	
	public void removeFromListHere(ProxiedPlayer player, ProxiedPlayer target) {
		if (plugin.getHereRequest().get(player.getName().toLowerCase()).contains(target.getName().toLowerCase())) {
			plugin.getHereRequest().get(player.getName().toLowerCase()).remove(target.getName().toLowerCase());
		}
		if(plugin.getHereRequest().get(player.getName().toLowerCase()).isEmpty()) {
			plugin.getHereRequest().remove(player.getName().toLowerCase());
		}
	}

}
